package user

import (
	"encoding/json"
	"github.com/jinzhu/gorm"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/middleware"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/models/user"
	"io"
	"io/ioutil"
	"net/http"
	// Here we import the sqlite3 dialect for GORM
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func handleGet(database *gorm.DB, w http.ResponseWriter, req *http.Request) {
	middleware.InfoLogger.Println("Inside the GET responder")

	var usersInResponse []user.User
	database.Limit(10).Find(&usersInResponse) // get 10 users

	b, _ := json.Marshal(map[string]interface{}{"users": usersInResponse, "code": http.StatusOK})
	io.WriteString(w, string(b))
	return
}

func handlePost(database *gorm.DB, w http.ResponseWriter, req *http.Request) {
	middleware.InfoLogger.Println("Inside the POST responder")

	bodyJSONByte, _ := ioutil.ReadAll(req.Body)

	var unmarshaledBody []user.User

	err := user.Unmarshal(bodyJSONByte, &unmarshaledBody)
	if err != nil {
		b, _ := json.Marshal(map[string]interface{}{"message": "Could not read Users from request", "code": http.StatusBadRequest})
		io.WriteString(w, string(b))
		return
	}

	for _, u := range unmarshaledBody {

		// First check that the users aren't already registered
		var bufferUser user.User
		database.Where("profile_url = ?", u.ProfileURL).First(&bufferUser)
		if bufferUser.ID != 0 {
			b, _ := json.Marshal(map[string]interface{}{"message": "User already registered", "code": http.StatusBadRequest, "users": bufferUser})
			io.WriteString(w, string(b))
			return
		}

		middleware.InfoLogger.Println("Inserting user into database:")
		middleware.InfoLogger.Println(u)
		database.Create(u)
	}

	var usersString string
	usersByte, _ := json.Marshal(unmarshaledBody)
	if string(usersByte) == "null" {
		usersString = "[]"
	} else {
		usersString = string(usersByte)
	}

	b, _ := json.Marshal(map[string]interface{}{"users": usersString, "code": http.StatusOK})
	io.WriteString(w, string(b))
}

//func handlePut(database *gorm.DB, w http.ResponseWriter, req *http.Request) {
//	middleware.InfoLogger.Println("Inside the PUT responder")
//
//	urlString := req.URL.RequestURI()
//
//	urlParts := strings.Split(urlString, "/")
//
//	if len(urlParts) != 3 {
//		middleware.ErrorLogger.Println("Problems parsing urlParts", urlParts)
//		b := response.NewResponse("Could not read user_id from request URI", http.StatusBadRequest)
//		io.WriteString(w, b)
//		return
//	}
//
//	if !bson.IsObjectIdHex(urlParts[2]) {
//		middleware.ErrorLogger.Println("Invalid user_id", urlParts[2])
//		b := response.NewResponse("Invalid user_id "+urlParts[2], http.StatusBadRequest)
//		io.WriteString(w, b)
//		return
//	}
//
//	userID := bson.ObjectIdHex(urlParts[2])
//
//	User := database.DB(config.DatabaseName).C(user.UserCollection)
//
//	bodyJSONByte, _ := ioutil.ReadAll(req.Body)
//
//	var unmarshaledBody []user.User
//
//	err := json.Unmarshal(bodyJSONByte, &unmarshaledBody)
//	if err != nil || len(unmarshaledBody) < 1 {
//		middleware.ErrorLogger.Println(err)
//		b := response.NewResponse("Could not read Users from request", http.StatusBadRequest)
//		io.WriteString(w, b)
//		return
//	}
//
//	// We expect a single user match in the search below
//	unmarshaledBody[0].ID = ""
//	err = User.UpdateId(userID, &unmarshaledBody[0])
//
//	if err != nil {
//		middleware.ErrorLogger.Println(err)
//		b := response.NewResponse("Could not update user in database", http.StatusInternalServerError)
//		io.WriteString(w, b)
//		return
//	}
//
//	var usersFound []user.User
//
//	findUser := User.FindId(userID).Select(bson.M{"password": 0, "salt": 0}).Iter()
//	err = findUser.All(&usersFound)
//
//	if err != nil {
//		middleware.ErrorLogger.Println(err)
//		b := response.NewResponse("Could not check user update in database", http.StatusInternalServerError)
//		io.WriteString(w, b)
//		return
//	}
//
//	if len(usersFound) <= 0 {
//		middleware.ErrorLogger.Println("Could not find user " + userID.Hex() + " in database")
//		b := response.NewResponse("Could not find the updated user in database", http.StatusNotFound)
//		io.WriteString(w, b)
//		return
//	}
//
//	middleware.InfoLogger.Println("Found the updated user:")
//	middleware.InfoLogger.Println(usersFound)
//
//	b, _ := json.Marshal(map[string]interface{}{"users": usersFound, "code": http.StatusOK})
//	io.WriteString(w, string(b))
//	return
//}
//
//func handleDelete(database *gorm.DB, w http.ResponseWriter, req *http.Request) {
//	middleware.InfoLogger.Println("Inside the DELETE responder")
//
//	urlString := req.URL.RequestURI()
//
//	urlParts := strings.Split(urlString, "/")
//
//	if len(urlParts) != 3 {
//		middleware.ErrorLogger.Println("Problems parsing urlParts", urlParts)
//		b := response.NewResponse("Could not read user_id from request URI", http.StatusBadRequest)
//		io.WriteString(w, b)
//		return
//	}
//
//	if !bson.IsObjectIdHex(urlParts[2]) {
//		middleware.ErrorLogger.Println("Invalid user_id", urlParts[2])
//		b := response.NewResponse("Invalid user_id "+urlParts[2], http.StatusBadRequest)
//		io.WriteString(w, b)
//		return
//	}
//
//	userID := bson.ObjectIdHex(urlParts[2])
//
//	User := database.DB(config.DatabaseName).C(user.UserCollection)
//
//	var usersFound []user.User
//
//	findUser := User.FindId(userID).Select(bson.M{"password": 0, "salt": 0}).Iter()
//	err := findUser.All(&usersFound)
//
//	if err != nil {
//		middleware.ErrorLogger.Println(err)
//		b := response.NewResponse("Could not find user update in database", http.StatusInternalServerError)
//		io.WriteString(w, b)
//		return
//	}
//
//	if len(usersFound) <= 0 {
//		middleware.ErrorLogger.Println("Could not find user " + userID.Hex() + " in database")
//		b := response.NewResponse("Could not find user update in database", http.StatusNotFound)
//		io.WriteString(w, b)
//		return
//	}
//
//	err = User.RemoveId(userID)
//
//	if err != nil {
//		middleware.ErrorLogger.Println(err)
//		b := response.NewResponse("Could not delete users in database", http.StatusInternalServerError)
//		io.WriteString(w, b)
//		return
//	}
//
//	middleware.InfoLogger.Println("Found and removed the user:")
//	middleware.InfoLogger.Println(usersFound)
//
//	b, _ := json.Marshal(map[string]interface{}{"users": usersFound, "code": http.StatusOK})
//	io.WriteString(w, string(b))
//	return
//}
//
//// Valid returns if a given userID corresponds to a User within the database
//func Valid(database *gorm.DB, userIDStr string) bool {
//	if !bson.IsObjectIdHex(userIDStr) {
//		return false
//	}
//	User := database.DB(config.DatabaseName).C(user.UserCollection)
//	var usersFound []user.User
//
//	userID := bson.ObjectIdHex(userIDStr)
//
//	findUser := User.FindId(userID).Iter()
//	err := findUser.All(&usersFound)
//
//	if err != nil {
//		return false
//	}
//
//	if len(usersFound) <= 0 {
//		return false
//	}
//
//	return true
//}

// User controller
func User(database *gorm.DB) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {

		middleware.InfoLogger.Println("Inside user controller")

		switch req.Method {
		case "GET":
			handleGet(database, w, req)
		case "POST":
			handlePost(database, w, req)
			//case "PUT":
			//handlePut(database, w, req)
			//case "DELETE":
			//handleDelete(database, w, req)
		}
	}
}
