# Comparison of Continuous Integration and Continuous Delivery Systems

## What are continuous integration(CI) and continuous delivery(CD) systems?

According to [Martin Fowler](https://martinfowler.com/articles/continuousIntegration.html) , one of the members of the Agile manifesto, Continuous Integration (CI),
> is a software development practice where members of a team integrate their work frequently, usually each person integrates at least daily - leading to multiple integrations per day. Each integration is verified by an automated build (including test) to detect integration errors as quickly as possible. Many teams find that this approach leads to significantly reduced integration problems and allows a team to develop cohesive software more rapidly.

The idea behind CI is that, whenever a code commit occurs, it triggers an automated system that checks how the changes introduced in the commit _integrate_ into the project without breaking its functionality.

[On the other hand](https://martinfowler.com/bliki/ContinuousDelivery.html), Martin Fowler defines Continuous Delivery (CD), as
>  a software development discipline where you build software in such a way that the software can be released to production at any time.

>You’re doing continuous delivery when,

>* Your software is deployable throughout its lifecycle
>* Your team prioritizes keeping the software deployable over working on new features
>* Anybody can get fast, automated feedback on the production readiness of their systems any time somebody makes a change to them
>* You can perform push-button deployments of any version of the software to any environment on demand

Thus, CI and CD are strictly related since whenever any change is made into the code, CI triggers checks to prove that those changes won't break functionality and CD deploys the new version into production enviroments.

In this document we will compare five continuous delivery systems,  

* Jenkins  
* GoCD  
* CircleCI  
* Travis-CI
* AWS CodePipelines 

## Jenkins

Jenkins is a self-contained, open source automation server which can be used to automate all sorts of tasks related to building, testing, and delivering or deploying software.   

### Main features

* First conceived as a Continuous Integration system, it evolved into a Continuous Delivery system
* Jenkins can run either,   
	+ With standard system packages   
	+ Within Docker containers   
	+ Standalone in machines with Java Runtime Environment (JRE)    
* Has a huge number of plugins, of interest are,
	+ Build integration using GitHub to trigger builds, besides, with aid of a plugin, Jenkins can pull code from any repository it has access to.   
        + Authentication integration using GitHub as the source of authentication information to secure a Jenkins instance.   
        + Extensibility is core to Jenkins. Its flexibility attributes itself to plugins being critical to Jenkins’ functionality. So it depends heavily on plugins for pretty much any use case.
* CI/CD configuration can be done either with a graphic dashboard or using a pipeline plugin
* With the Pipeline plugin users can implement a project’s entire build/test/deploy pipeline in a Jenkinsfile and store that alongside their code, treating their pipeline as another piece of code checked into source control.  
* Jenkins can be deployed on your own servers, on the cloud, even is offered as an hosted service by some companies
* CD is achieved by the use of plugins

## GoCD

* GoCD aims to support the most common CD scenarios out of the box without any plugin installation, built on the principles of Continuous Delivery.   
* Although GoCD is built specifically with CD in mind, it has sophisticated features for continuous integration.   
* GoCD incorporates its own pipelines,
	+ The GoCD server does not perform work on its own, it dependes on the _agents_.   
	+ The _agents_ are the entities responsible to performing any task (run commands, do deployments, etc.).   
	+ The _pipeline_ represents a workflow or part of a workflow.   
	+ Pipelines are triggered by _materials_, typically materials are source control repositories (like git, mercurial, subversion, etc.)   
	+ Agents are the responsible of running workflows declare on pipelines.   
* GoCD offers a graphical dashboard when agents and pipelines are declared.   
* It is possible to configure pipelines with GoCD's template system. Templates support different formats like JSON or YAML   
* GoCD includes a generous set of plugins,
	+  To enable integration with other systems and deployment to docker containers or publish/fetch from AWS S3 buckets
	+ Notification plugins via email, websockets, Gitter, AWS SNS, Slack integrations, and much more
* GoCD must be run on a server (either if this server is a virtual machine on the cloud or a private server), we don't know hosted GoCD providers.   

## CircleCI

* CircleCI integrates with GitHub, GitHub Enterprise, and Bitbucket. Every time you commit code, CircleCI creates a build.   
* CircleCI automatically tests your build in a clean container or virtual machine. Any docker image, Linux VM, or mac OS VM is supported.     
* Your team is notified if a build fails so issues can be fixed quickly.   
* Passing builds are deployed to various environments so your product goes to market faster.   
* CircleCI is language agnostic, if it can run on Linux, it can be deployed with CircleCI.   
* Supports parallel or sequential jobs.   
* It is hosted on the cloud (Integrating against Github or Bitbucket), or can run on paid private servers (private servers only integrate with Github).   

## Travis-CI

* Exclusively integrates with Github.    
* Code commits trigger TravisCI builds, which notify of build completion/failure and can deploy to heroku, AWS, Google Cloud, [among a long list of other popular options](https://docs.travis-ci.com/user/deployment)   
* Travis CI offers a few different infrastructure environments, so you can select the setup that suits your project best:
	+ Container-based, a Linux Ubuntu environment running in a Docker container.   
	+ Sudo-enabled - this Linux Ubuntu environment runs on full virtual machine. It starts a little slower, but it has more computational resources, and supports the use of sudo, setuid, and setgid.   
	+ OS X - uses one of several versions of the OS X operating system. This environment is useful for building projects that require the OS X software, such as projects written in Swift.    

## AWS CodePipelines

* AWS CodePipeline is a continuous delivery service you can use to model, visualize, and automate the steps required to release your software.  
* In AWS CodePipeline, a _pipeline_ is a workflow construct that describes how software changes go through a release process.   
* AWS CodePipeline breaks up your workflow into a series of stages, where every stage contains at least one action.   
* You can use the AWS CodePipeline console, the AWS Command Line Interface (AWS CLI), the AWS SDKs, or any combination of these to create and manage your pipelines.   
* AWS CodePipeline fully automates the release process from end to end, starting from the source repository through build, test, and deployment.  
* It is possible to incorporate existing source, build, and deployment tools into the pipeline.      
* Reviewing the real-time status of the pipelines, check the details of any alerts, retry failed actions, view details about the source revisions used in the latest pipeline execution in each stage, and manually rerunning any pipeline is possible with the AWS CodePipelines console    

------------------

## References
1. [https://jenkins.io/](https://jenkins.io/)  
2. [https://www.gocd.org/](https://www.gocd.org/)   
3. [https://circleci.com/product/](https://circleci.com/product/)  
4. [https://highops.com/insights/continuous-delivery-pipelines-gocd-vs-jenkins/](https://highops.com/insights/continuous-delivery-pipelines-gocd-vs-jenkins/)  
5. [https://www.gocd.org/jenkins/](https://www.gocd.org/jenkins/)  
6. [https://docs.travis-ci.com/](https://docs.travis-ci.com/)
7. [https://docs.aws.amazon.com/codepipeline/latest/userguide/welcome.html](https://docs.aws.amazon.com/codepipeline/latest/userguide/welcome.html)
8. [https://martinfowler.com/articles/continuousIntegration.html](https://martinfowler.com/articles/continuousIntegration.html)  
9. [https://martinfowler.com/bliki/ContinuousDelivery.html](https://martinfowler.com/bliki/ContinuousDelivery.html)  