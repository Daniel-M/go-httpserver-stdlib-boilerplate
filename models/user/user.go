package user

import (
	"encoding/json"
	"github.com/jinzhu/gorm"
	"math/rand"
	"time"
	// Here we import the sqlite3 dialect for GORM
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// User is the example model for the Database
type User struct {
	gorm.Model
	Name       string `gorm:"name" json:"name" bson:"name"`
	ProfileURL string `gorm:"profile_url" json:"profile_url" bson:"profile_url"`
}

// NewUser returns a new user
// Based upon this logic https://play.golang.org/p/g39gXs1fSw6
func NewUser(Name,
	ProfileURL string) *User {
	user := new(User)

	user.Name = Name
	user.ProfileURL = ProfileURL

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	user.ID = uint(r.Uint32())

	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()

	return user
}

// Unmarshal tries to unmarshal the contents of data as an array of Users in
// compliance with the User struct,  if err != nil the marshaling failed,
// otherwise the array outUsers will have the array of unmarshalled Users
func Unmarshal(data []byte, outUsers *[]User) error {
	var bufferUser []User
	err := json.Unmarshal(data, &outUsers)

	if err != nil {
		return err
	}

	for _, u := range *outUsers {
		r := rand.New(rand.NewSource(time.Now().UnixNano()))
		u.ID = uint(r.Uint32())

		u.CreatedAt = time.Now()
		u.UpdatedAt = time.Now()
		bufferUser = append(bufferUser, u)
	}

	*outUsers = bufferUser

	return nil
}
