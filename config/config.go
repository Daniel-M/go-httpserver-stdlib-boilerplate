package config

// DatabaseName name of the database
const DatabaseName = "mydatabase.db"

// DatabasePort the port to attempt connection with database
const DatabasePort = "27017"

// DatabaseHost IP to the database host
const DatabaseHost = "localhost"

const databaseUser = "myuser"
const databasePassword = "mypass"

// ServerPort port at which the API will be served
const ServerPort = ":5550"

const databaseAuthentication = DatabaseName

//const databaseOptions  = "authSource=" + databaseAuthentication + "&ssl=true"
const databaseOptions = "authSource=" + databaseAuthentication

// DatabaseProtocol the protocol/dialect of the database used
const DatabaseProtocol = "sqlite3"

// JwtSigningKey is the key to sign the generate all JWT tokens
const JwtSigningKey = "ultraSecret"

// JwtIssuer the issuer for the JWT tokens
const JwtIssuer = "Daniel-M"

// JwtExpiration sets the expiration time for the tokens
// 1 day * 24 hours * 3600 seconds per hour
const JwtExpiration = int64(1 * 24 * 3600)
