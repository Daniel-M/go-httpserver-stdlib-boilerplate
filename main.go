package main

import (
	//"encoding/json"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/config"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/controllers/user"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/middleware"
	userModel "gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/models/user"
	//"fmt"
	"github.com/jinzhu/gorm"
	//"io"
	//"io/ioutil"
	"log"
	"net/http"
	//"os"
)

func main() {

	middleware.InfoLogger.Println("Attempting connection with database:", config.DatabaseProtocol)

	db, err := gorm.Open(config.DatabaseProtocol, config.DatabaseName)
	if err != nil {
		panic("Failed to connect database")
	}
	defer db.Close()

	middleware.InfoLogger.Println("Connection with database established")

	// Migrate the schema
	db.AutoMigrate(&userModel.User{})

	mux := http.NewServeMux()

	// We can apply selectively the Jwt middleware. Here is disabled
	mux.HandleFunc("/", middleware.Chain(user.User(db), middleware.Logger()))

	// Here we added the Jwt middleware
	mux.HandleFunc("/users/", middleware.Chain(user.User(db), middleware.Jwt(db), middleware.Logger()))

	middleware.InfoLogger.Println("Listening at port", config.ServerPort)

	//http.ListenAndServe(config.ServerPort, mux)

	log.Fatalf("FATAL: %d %s ", log.Ldate|log.Ltime, http.ListenAndServe(config.ServerPort, mux))
}
