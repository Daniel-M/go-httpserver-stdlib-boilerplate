package password

import (
	"crypto/rand"
	"encoding/base64"
	"golang.org/x/crypto/bcrypt"
)

const randomStringBytes = 16
const hashingCost = 10

// HashPassword creates a hash with password. The hash is generated using a
// random salt of numberOfBytes bytes long.
func HashPassword(password string, numberOfBytes int) (hash, salt string, err error) {
	if numberOfBytes <= 2 {
		numberOfBytes = randomStringBytes
	}
	salt, err = GenerateRandomString(numberOfBytes)
	bytes, err := bcrypt.GenerateFromPassword([]byte(password+salt), hashingCost)
	hash = string(bytes)
	return
}

// CheckPasswordHash checks if password with the given salt corresponds to the
// the hash
func CheckPasswordHash(password, salt, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password+salt))
	return err == nil
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	bytesRead := make([]byte, n)
	_, err := rand.Read(bytesRead)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return bytesRead, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
func GenerateRandomString(bytesLong int) (string, error) {
	randomBytes, err := GenerateRandomBytes(bytesLong)
	return base64.URLEncoding.EncodeToString(randomBytes), err
}

//func sample() {
//	myPwd := "abc123"
//	providedHash, salt, _ := HashPassword(myPwd, 32)
//	fmt.Println("Password :", myPwd)
//	fmt.Println("Salt :", salt)
//	fmt.Println("Hash :", providedHash)
//
//	isMatch := CheckPasswordHash(myPwd, salt, providedHash)
//	fmt.Println("Matched ?:", isMatch)
//}
