package jwt

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/config"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/models/user"
	"time"
)

// GenerateJwt generates a signed JWT Token provided the subject
func GenerateJwt(subject string) (tokenStr string, err error) {
	signingKey := []byte(config.JwtSigningKey)

	now := time.Now().Unix()

	// Create the Claims
	claims := &jwt.StandardClaims{
		IssuedAt:  now,
		ExpiresAt: now + config.JwtExpiration,
		Issuer:    config.JwtIssuer,
		Subject:   subject,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err = token.SignedString(signingKey)
	return
}

// ValidateToken validates an input token tokenStr
func ValidateToken(database *gorm.DB, tokenStr string) bool {
	token, err := jwt.ParseWithClaims(tokenStr, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.JwtSigningKey), nil
	})

	if err != nil {
		//panic(er)
		return false
	}

	//fmt.Println("parsed token")
	//fmt.Println(token)
	//fmt.Println("parsed Claim.Subject")

	claims, ok := token.Claims.(*jwt.StandardClaims)
	userID := claims.Subject

	//fmt.Println(reflect.TypeOf(claims).String())

	//fmt.Println(ok)
	//fmt.Println(claims)
	//fmt.Println(claims.Subject)

	if !ok {
		return false
	}

	var findUser user.User
	database.First(&findUser, userID)

	fmt.Println("Found user:")
	fmt.Println(findUser)

	return true
}
