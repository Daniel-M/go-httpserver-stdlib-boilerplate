package middleware

import (
	//"bytes"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/security/jwt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

// Middleware middleware type
type Middleware func(f http.HandlerFunc) http.HandlerFunc

// InfoLogger decorates logging
var InfoLogger = log.New(os.Stdout,
	"INFO: ",
	log.Ldate|log.Ltime)

// ErrorLogger decorates logging
var ErrorLogger = log.New(os.Stdout,
	"Error: ",
	log.Ldate|log.Ltime)

// Jwt middleware
func Jwt(database *gorm.DB) Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {

			// Check the token in the header
			headerToken := r.Header.Get("Authorization")
			queryMap := r.URL.Query()
			//fmt.Println(headerToken)
			//fmt.Println(queryMap["auth"])

			// Get request
			if r.Method == "GET" && (len(queryMap["auth"]) <= 0 && headerToken == "") {
				b, _ := json.Marshal(map[string]interface{}{"message": "No token credentials found on the query string or request headers",
					"code": http.StatusUnauthorized})
				io.WriteString(w, string(b))
				return
			}

			if r.Method != "GET" && headerToken == "" {
				b, _ := json.Marshal(map[string]interface{}{"message": "No token credentials found on the request headers",
					"code": http.StatusUnauthorized})
				io.WriteString(w, string(b))
				return
			}

			var token string

			if headerToken != "" {
				token = strings.Split(headerToken, " ")[1]
			} else if len(queryMap["auth"]) == 1 {
				token = queryMap["auth"][0]
			}

			//fmt.Println("jwt.ValidateToken(database, token)")
			//fmt.Println(jwt.ValidateToken(database, token))
			if !jwt.ValidateToken(database, token) {
				b, _ := json.Marshal(map[string]interface{}{"message": "Please provide a valid token",
					"code": http.StatusUnauthorized})
				io.WriteString(w, string(b))
				return
			}

			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}

// Logger logging middleware
func Logger() Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			// time stamp
			start := time.Now()

			//
			InfoLogger.Println("--->", r.Method)
			InfoLogger.Printf("%+v", *r)

			// defer the function execution just after
			// f(w, r) ends
			defer func() {
				InfoLogger.Println("<---", r.Method, r.URL.Path, time.Since(start))
			}()
			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}

// Chain applies middlewares to a http.HandlerFunc
func Chain(f http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {
	for _, m := range middlewares {
		f = m(f)
	}
	return f
}
