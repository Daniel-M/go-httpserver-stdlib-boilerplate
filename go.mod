module bitbucket.org/Daniel-M/goploy

require (
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a
	github.com/mattn/go-sqlite3 v1.10.0
)
