package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/config"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/controllers/user"
	"gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/middleware"
	userModel "gitlab.com/Daniel-M/go-httpserver-stdlib-boilerplate/models/user"
	"log"
	//"io"
	"io/ioutil"
	//"net/http"
	"net/http/httptest"
	"os/exec"
	"testing"
)

func TestPostMethod(t *testing.T) {
	db, err := gorm.Open(config.DatabaseProtocol, config.DatabaseName)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&userModel.User{})

	data := []userModel.User{userModel.User{Name: "Daniel-M", ProfileURL: "http://www.github.com/Daniel-M"}}
	body, _ := json.Marshal(data)

	var urlRequest = "http://" + config.DatabaseHost + config.ServerPort + "/"
	middleware.InfoLogger.Println("Attempting request to ", urlRequest)

	req := httptest.NewRequest("POST", urlRequest, bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	user.User(db)(w, req)

	res := w.Result()
	bodyByte, _ := ioutil.ReadAll(res.Body)

	var bodyJSON map[string]interface{}

	err = json.Unmarshal(bodyByte, &bodyJSON)

	if err != nil {
		log.Fatal("Something happened parsing the response:", err)
	}

	if bodyJSON["code"].(float64) != 200 {
		t.Fatal("The user handler sent the wrong response code", string(bodyByte))
	}
}

// TestGetMethod tests that the Server sends the correct response for the
// GET query
func TestGetMethod(t *testing.T) {
	db, err := gorm.Open(config.DatabaseProtocol, config.DatabaseName)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	req := httptest.NewRequest("GET", "http://localhost:5555/", nil)
	w := httptest.NewRecorder()

	user.User(db)(w, req)

	res := w.Result()
	bodyByte, _ := ioutil.ReadAll(res.Body)

	var bodyJSON map[string]interface{}

	err = json.Unmarshal(bodyByte, &bodyJSON)

	// Delete the temporary config.DatabaseName file
	cmd := exec.Command("rm", "-rfv", config.DatabaseName)
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	if err != nil {
		log.Fatal("There was an error deleting the file" + config.DatabaseName)
		log.Fatal(err)
	}
	text := fmt.Sprintf("%s", out.String())
	middleware.InfoLogger.Println(text)

	if err != nil {
		log.Fatal("Something happened parsing the response:", err)
	}

	if bodyJSON["code"].(float64) != 200 {
		t.Fatal("Handler sent the wrong response code", string(bodyByte))
	}

}
